const { dialog, app, BrowserWindow, Tray, Menu } = require('electron')
const windowStateKeeper = require('electron-window-state')
const contextMenu = require('electron-context-menu')
const store = require('electron-store')
const ipc = require('electron').ipcMain
const fs = require('fs')
const { autoUpdater }  = require('electron-updater')

const schema = {
  username: {
    type: 'string',
    default: ''
  },
  password: {
    type: 'string',
    default: ''
  },
  startInTray: {
    type: 'boolean',
    default: false
  },
  closeToTray: {
    type: 'boolean',
    default: true
  }
}

/*autoUpdater.on('update-available', () => {
  dialog.showMessageBox({
    type: 'info',
    title: 'Update Available',
    message: 'An update for Converse is available, do you want update now?',
    buttons: ['Yes', 'No']
  }, (buttonIndex) => {
    if (buttonIndex === 0) {
      autoUpdater.downloadUpdate()
    }
  })
})

/*autoUpdater.on('update-downloaded', () => {
  dialog.showMessageBox({
    type: 'info',
    title: 'Update Ready',
    message: 'Converse will restart after applying the update.',
    buttons: ['OK']
  }, (buttonIndex) => {
    autoUpdater.quitAndInstall()
  })
})*/

const config = new store({schema})
//autoUpdater.autoDownload = false
let tray = null
let win = null

function sleep(ms){
  return new Promise(resolve=>{
      setTimeout(resolve,ms)
  })
}

async function updateLoop() {
  autoUpdater.checkForUpdates()
  await sleep(7200000)
  updateLoop()
}

function createWindow () {
  updateLoop()
  if(config.store.closeToTray){
    //create system tray entry
    tray = new Tray(__dirname+'/tray.png')
    //build tray menu
    tray.setContextMenu(Menu.buildFromTemplate([
      {
        label: 'Show App', click: function () {
          win.show();
        }
      },
      {
        label: 'Quit', click: function () {
          win.destroy();
          app.quit();
        }
      }
    ]));
    tray.setToolTip('Converse')
    tray.on('click', () => {
      if(win.isVisible()) win.hide()
      else win.show()
    })
  }
  // Create the browser window.
  let mainWindowState = windowStateKeeper({
    defaultWidth: 1000,
    defaultHeight: 800
  });
  win = new BrowserWindow({
    x: mainWindowState.x,
    y: mainWindowState.y,
    width: mainWindowState.width,
    height: mainWindowState.height,
    show: !(config.store.startInTray && config.store.closeToTray),
    webPreferences: {
      nodeIntegration: false,
      preload: __dirname+'/preload.js'
    },
    icon: __dirname+'/icon.png'
  })
  //disable default menubar
  win.setMenuBarVisibility(false)
  //manage & remember window size/state
  mainWindowState.manage(win)
  if(config.store.closeToTray){
    //intercept close and hide to tray
    win.on('close', function (event) {
      event.preventDefault();
      win.hide();
    });
  }
  //load a sane default context menu
  contextMenu({
    showSaveImageAs: true,
    showInspectElement: false
  });
  //taskbar flash on notifs
  ipc.on('notification-shim', (e, msg) => {
    win.flashFrame(true)
    if(tray && !win.isVisible()) tray.setImage(__dirname+'/trayNotif.png')
  });
  //disable on focus
  win.on('focus', () => {
    win.flashFrame(false)
    if(tray) tray.setImage(__dirname+'/tray.png')
  })
  // and load the index.html of the app.
  win.loadURL('file://'+__dirname+'/index.html?jid='+encodeURIComponent(config.store.username)+'&pass='+encodeURIComponent(config.store.password))
}

app.on('ready', createWindow)
